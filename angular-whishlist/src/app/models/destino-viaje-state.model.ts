import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';
import { type } from 'os';
import { stat } from 'fs';

export interface DestinoViajeState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export const intializeDestinosViajeState = function() {
    return {
        items: [],
        loading: false,
        favorito: null
    };
}

export enum DestinoViajeActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito'
}

export class NuevoDestinoAction implements Action{
    type = DestinoViajeActionTypes.NUEVO_DESTINO;
    constructor(public destino:DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action{
    type = DestinoViajeActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) {}
}

export type DestinoViajeActions = NuevoDestinoAction | ElegidoFavoritoAction;

export function reducerDestinosViajes(
    state: DestinoViajeState,
    action: DestinoViajeActions
): DestinoViajeState {
    switch (action.type) {
        case DestinoViajeActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino ]
            };
        }
        case DestinoViajeActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => x.setSelected(false));
            const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            };
        }
    }
    return state;
}

@Injectable()
export class DestinosViajeEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinoViajeActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );
    constructor(private actions$: Actions) {}
}
